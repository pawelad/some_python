from datetime import date


# This is a simple class that represents a person
class Person:
    def __init__(self, first_name, last_name, date_of_birth):
        # This is a very common occurrence in Python classes
        # We assign the variables passed in class initialisation to the class instance
        # (available to us via first function argument, which should always be named `self`)
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth


# This is how we initialize a class *instance*
john = Person("John", "Cleese", date(1939, 10, 27))
eric = Person("Eric", "Idle", date(1943, 3, 29))

# Both variables are *instances* of `Person` class
# (which is the same as saying "the type of that variable is `Person`")
john_type = type(john)  # -> <class '__main__.Person'>
foobar1 = isinstance(john, Person)  # -> True
eric_type = type(eric)  # -> <class '__main__.Person'>
foobar2 = isinstance(john, Person)  # -> True
