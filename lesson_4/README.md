# Lesson 4 - Classes

## Classes
| Code  | [a_classes.py](a_classes.py)                    |
|-------|-------------------------------------------------|
| Other | https://docs.python.org/3/tutorial/classes.html |

Classes are the last "building block" we're going to cover, and help people model, encapsulate and
abstract certain patterns from the real world.

From the [Python docs](https://docs.python.org/3/tutorial/classes.html):
> Classes provide a means of bundling data and functionality together. Creating a new class creates
> a new _type_ of object, allowing new _instances_ of that type to be made. Each class instance can
> have attributes attached to it for maintaining its state. Class instances can also have methods
> (defined by its class) for modifying its state.

Classes have a number of "magic" methods (identifiable by the `__` prefix and suffix) that change
its behaviour. The most important of them is `__init__`, which defines which arguments the class
takes during its initialisation.

A simple example of a class would be:

```python
from datetime import date

class Person:
    def __init__(self, first_name, last_name, date_of_birth):
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth

john = Person("John", "Cleese", date(1939, 10, 27))
eric = Person("Eric", "Idle", date(1943, 3, 29))
```

The first argument of all class methods is special and contains the _instance_ of the class. By
convention, it should always be named `self` and can be used to access other class attributes and
methods.

## Class methods
| Code  | [b_class_methods.py](b_class_methods.py) |
|-------|------------------------------------------|
| Other |                                          |

Methods are special functions that are defined _inside_ a class and operate on their _instances_.
They're accessed via `.` operator, for example `foobar.baz()` (which would call the `baz` method on
whatever class `foobar` is an instance of).

For example all strings have an `upper()` _method_ that returns the upper case version of the
string, and a _method_ called `.isupper()` that returns `True` if all characters in a string are
upper case (and `False` otherwise):

```python
foobar1 = "I am a string variable"
print(foobar1.isupper())  # -> False
foobar2 = foobar1.upper()
print(foobar2)  # -> "I" AM A STRING VARIABLE"
print(foobar2.isupper())  # -> True
```

Methods can also _change_ the variable they're operating on - for example list's `.append()` method
adds passed argument to the list (in place).

```python
foobar = ["one item"]
foobar.append("second item")
print(foobar) # ["one item", "second item"]
```

OK, so these are built-in methods, for built-in classes. But we can also create methods for classes
we wrote:

```python
from datetime import date, timedelta

class Person:
    def __init__(self, first_name, last_name, date_of_birth):
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth

    def age(self):
        """
        Calculate person age.
        
        Source:
            https://stackoverflow.com/a/4828842/3023841
        """
        return (date.today() - self.date_of_birth) // timedelta(days=365.2425)

    
john = Person("John", "Cleese", date(1939, 10, 27))
eric = Person("Eric", "Idle", date(1943, 3, 29))

print(john.age())  # -> 83
print(eric.age())  # -> 79
```
