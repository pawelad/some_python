from datetime import date, timedelta

# We've actually used methods before, we just didn't call them that
today = date.today()
foobar1 = ["1", 2]
foobar1.append(3)  # -> ["1", 2, 3]


# We can also create methods on classes we create
class Person:
    def __init__(self, first_name, last_name, date_of_birth):
        self.first_name = first_name
        self.last_name = last_name
        self.date_of_birth = date_of_birth
        self.friends = []

    def age(self):
        """
        Calculate person age.

        Source:
            https://stackoverflow.com/a/4828842/3023841
        """
        return (date.today() - self.date_of_birth) // timedelta(days=365.2425)

    def add_friend(self, person):
        self.friends.append(person)


john = Person("John", "Cleese", date(1939, 10, 27))
eric = Person("Eric", "Idle", date(1943, 3, 29))

# This is how we call the `age` method
john_age = john.age()  # -> 83
eric_age = eric.age()  # -> 79

# Just like regular functions, methods can also have other arguments
foobar1 = john.friends  # -> []
john.add_friend(eric)
foobar2 = john.friends  # -> [<__main__.Person object at 0x10cfbc4f0>]
