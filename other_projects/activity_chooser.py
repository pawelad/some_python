import random

import questionary


print("Choose! The! Activity!")

while True:
    # ask the user to provide a list of possible activities
    print("Please input activities to choose from (separated by a comma):")
    # TODO: we should let the user input line by line, until he preses Enter twice in a row
    activities_input = input()

    activities = []
    for activity in activities_input.split(","):
        activity = activity.strip()  # get rid of whitespace
        activities.append(activity)

    if len(activities) < 2:
        print("Please input at least two activities")
        continue

    activity1 = random.choice(activities)
    activity2 = random.choice(activities)

    # make sure the activities are different
    while activity1 == activity2:
        activity2 = random.choice(activities)

    choices = [activity1, activity2]
    user_answer = questionary.select("Which do you prefer?", choices=choices).ask()

    print(f"{user_answer=}")

    # print("Choose one of these:")
    # print(f"a) {activity1} vs. b) {activity2}")
    # answer = input()

    break
