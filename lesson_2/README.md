# Lesson 2 - Still basics, but a bit more advanced

## F-strings
| Code  | [a_f_strings.py](a_f_strings.py) |
|-------|----------------------------------|
| Other |                                  |

F-strings are a somewhat new Python feature (3.6+) that allow _embedding_ variables inside strings.
For example, instead of doing `print("Hello " + name + "!")` you can prefix the string with `f`
(which tells Python it's an f-string) and then _access_ variables via `{}` brackets.

An f-string equivalent to the example above would be `print(f"Hello {name}!")`.

## Loops
| Code  | [b_loops.py](b_loops.py) |
|-------|--------------------------|
| Other |                          |

Loops are another basic building block of almost all programs.

A `for` loop _iterates_ over a list (or rather, a container). For example `for n in [1, 2, 3]:`
would loop three times, with variable `n` having value `1`, `2` and `3` in each _iteration_.

If you'd like to do something N times, the easiest way is to do `for n in range(n):` - `range()` is
a builtin functions that creates a list with N elements (with values 0 to N-1).

A `while` loop iterates as long as the passed expression _evaluates_ to true - for example
`while n < 5` will run _until_ n is equal to 5 or greater.

To skip the remaining portion of the loop and immediately go to the next iteration, use `continue`.
If you want to break out of the loop altogether, use `break`.

## Exceptions
| Code  | [c_exceptions.py](c_exceptions.py) |
|-------|------------------------------------|
| Other |                                    |

Think of exceptions as _errors_ that are _raised_ when running a program. It basically means that
something we weren't prepared for (or is simply impossible) happened. For example, trying to divide
any number by 0 will throw a `ZeroDivisionError`, adding an integer to a string will result in a
`TypeError` and trying to access a non-existent list element causes `IndexError`.

The good news - you can _catch_ and _handle_ exceptions. We do that with
`try: ... except Exception: ...` syntax, with an optional `else:` part that is executed when an
exception wasn't raised.

For example, built in `input` function takes user input from the terminal and returns it. But, it
always returns a string. If you want the user to enter a number, you could do something like this:

```python
user_guess = None
while not isinstance(user_guess, int):
    print("Enter a number:")
    user_guess = input()

    try:
        user_guess = int(user_guess)
    except ValueError:
        print(f"'{user_guess}' is not a number")
    else:
        print(f"Thank you, {user_guess} is a great number")
```

## Functions
| Code  | [d_functions.py](d_functions.py) |
|-------|----------------------------------|
| Other |                                  |

A function is a _reusable_ piece of code that can (but doesn't have to) accept arguments and returns
a value (by default it returns `None`). Arguments can also have default values.

We already learned about a bunch of _builtin_ functions, like `print()`, `type()`, `bool()`. We also
imported `randint` function from `random` module, but most importantly, you can also define your own
functions:

```python
def sum(a, b):
    return a + b
```

## Homework
- create a function that given a list of integers, returns its average
