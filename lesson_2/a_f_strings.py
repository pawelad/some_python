# these two are doing the same thing, but one is (clearly) nicer then the other one
name = "Paweł"
print("Hello " + name + "!")
print(f"Hello {name}!")

# what's more, you can even do things like this
print(f"2 + 2 is equal to {2 + 2}")
