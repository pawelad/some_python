# you can catch exceptions and handle them
# all unhandled exception exit the program
try:
    a = 5 / 0
except ZeroDivisionError:
    print("but... you can't divide by zero!")

# builtin 'input' function is used to get user input from the terminal
foobar1 = input()  # this will be equal to whatever you input in the terminal

# using a while loop, input function and exceptions to get a number from a user
user_guess = None
while not isinstance(user_guess, int):
    print("Enter a number:")
    user_guess = input()

    try:
        user_guess = int(user_guess)
    except ValueError:
        print(f"'{user_guess}' is not a number")
    else:
        print(f"Thank you, {user_guess} is a great number")
