# here are some builtin functions we previously used
print("test")
str(1)
type(1)

# we can also import functions from the standard library
from datetime import datetime

# or, we can define our own functions
def print_now():
    print(datetime.now())

print_now()

# all functions return 'None' by default
def empty_function():
    2 + 2

foobar1 = empty_function()  # -> None

# functions also take arguments, which can have default values
def sum(a, b=2):
    return a + b

foobar2 = sum(5)  # -> 7
foobar3 = sum(50, 5)  # -> 55
