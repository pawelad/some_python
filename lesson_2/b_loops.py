# let's create an example list
too_hot = ["manuela", "lecho", "nico", "flor", "carolina", "israel", "ania g"]
foobar1 = too_hot[2]  # -> "nico"
foobar2 = "manuela" in too_hot  # -> True
foobar3 = len(too_hot)  # -> 7

# for loop iterates over elements of the passed list (container)
# in each iteration name variable will be set to the next element of the list
for name in too_hot:
    print(f"Hello {name}")

# you can also iterate on strings, which will iterate over characters of the string
example_str = "Ahoy matey"
for char in example_str:
    print(char)

# 'range' function takes one argument and returns a list of numbers from 0 to n-1
# you can use 'range' to do something n times
n = 5
for m in range(n):
    print(f"I'm gonna print this {m} times")

foobar4 = list(range(3)) == [0, 1, 2, 4]  # -> True

# while loop runs until the condition stops evaluating to True
n = 5
while n < 10:
    print("a is smaller then 10")
    n = n + 1

# continue is used to skip the rest of the loop and immediately go to the next iteration
for name in too_hot:
    if name != "carolina":
        continue

    print(f"Hello {name}, you are beautiful")


# (which could be actually be written nicer)
for name in too_hot:
    if name == "carolina":
        print(f"Hello {name}, you are beautiful")

# break is used to immediately get out of the loop
while True:
    if n == 15:
        print("a is equal to 15")
        break

    n = n + 1

print("I'm out of the loop!")
