# Some Python
My semi-organized notes from teaching some Python to my friends.

Very unstructured. Mostly created on the spot. Some thought given. A work in progress.

## Getting started
From experience, Python installation and the concept of "weird black text editor" (otherwise known
as "The Terminal") scares beginners a bit. Instead of starting with all that, I like to use the
awesome [Mu editor] which (among other very cool things) installs Python for you, and is built with
new programmers in mind.

If you feel like you "outgrown" Mu, give [VS Code] a chance.

## Other Python resources

- [Official Python docs]
- [Online iPython shell]
- [Learn Python Tutorial]
- [30 Days Of Python]

## Exercises
If you already went through all the lessons and just want to practice, feel free to pick one of
these exercises:

- create a function that takes a list of numbers and returns its median value
- create a function that takes a string and censures popular curse words by replacing them with `*`
- create a function that takes a string and returns a number of vowels in it
- create a function that takes a number and returns `True` if it's a prime number, `False` otherwise
- create a function that takes a number and returns first prime number bigger then it
- create a function that takes two lists and returns a list of elements present in both of them
- create a function that takes a string and returns `True` if it's a palindrome, `False` otherwise
- create a function that takes a year and returns all Fridays the 13th
- create a function that takes a number and draws an ASCII square of that size

## To do
Topic ideas:
- list slices
- enumerate
- class inheritance
- git
- GitHub
- testing with pytest
- databases
- Pygame Zero
- Flask
- type annotations
- dataclasses
- installing packages from PyPI

Mini project ideas:
- Game of Life


[30 Days Of Python]: https://github.com/Asabeneh/30-Days-Of-Python
[Learn Python Tutorial]: https://www.learnpython.org/en/Welcome
[Mu editor]: https://codewith.mu/
[Official Python docs]: https://docs.python.org/3/
[Online iPython shell]: https://www.pythonanywhere.com/try-ipython/
[VS Code]: https://code.visualstudio.com/
