# import 'example_int' variable from 'b_variables' module
from b_variables import example_int

# this _compares_ two values and returns True if they are the same, and False if they are not 
foobar1 = example_int == 5  # -> True

# interestingly, True is _equal_ to 1 and False is equal to 0
foobar2 = True == 1  # -> True
foobar3 = False == 0  # -> True

foobar4 = example_int < 5  # -> False
foobar5 = example_int >= 5  # -> True
foobar6 = 8 < example_int < 22  # -> False
# which is actually the same as
foobar7 = 8 < example_int and example_int < 22  # -> False

# some values are considered _truthy_ or _falsy_
foobar8 = bool("")  # -> False
foobar9 = bool("False")  # -> True

foobar10 = bool(0)  # -> False
foobar11 = bool(-11)  # -> True

# this is how you check if value is in a list (or more properly, in a container)
foobar12 = example_int in [2, 4, 8, 16]  # -> False

# Python also has three logical keywords, 'and', 'or' and 'not'
foobar13 = (6 < 3) and (7 == 7)  # -> False and True -> False
foobar14 = (6 > 3) or (7 == 7)  # -> True and True -> True
foobar15 = not False  # -> True

# if statements use boolean logic to decide which part of the code should be executed
happiness = 7.5
if happiness <= 3:
    print("😭")
elif 3 < happiness <= 6:
    print("😶")
elif 7 < happiness <= 9:
    print("😁")
else:
    print("🔥")
