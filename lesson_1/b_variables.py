# int / integer
example_int = 2
my_age = 27
foobar1 = type(my_age)  # -> <class 'int'>
foobar2 = my_age * example_int - example_int  # -> 52

# float / double
example_float = 1.5
my_height = 1.86
foobar3 = isinstance(my_height, int)  # True
foobar4 = example_float / my_height  # -> 0.8064516129032258

# str / string
example_str = "yo mama"
my_nickname = "pawelad"
# interestingly enough, you can 'add' and 'multiply' strings
foobar5 = example_str + " " + my_nickname   # -> "yo mama pawelad"
foobar6 = example_str * 2  # -> "yo mamayo mama"

# also, you can also do this, which means the 4th character of the string
_ama = my_nickname[4]  # -> l
meo_ = my_nickname[example_int]  # -> w

# bool / boolean
example_bool = True
is_rich = False

# None
example_none = None
boob_size = None

# list
example_list = [1, 2, 3, "foobar", True, False, 2.5]
bffs = ["Mateusz", "Karolina"]
# you can access list elements like that, with the index starting at 0
mati = bffs[0]  # -> "Mateusz"
# accessing elements with index bigger then the number of elements will cause an error
# bffs[3] <- doesn't work (raises a IndexError exception)

# dict / dictionary / hashmap
example_dict = {
    "key": "value",
    "dupa": True,
    1: "value2",
}
pawelad = {
    "name": "Paweł",
    "surname": "Adamczak",
    "my_age": my_age,
    "my_height": my_height,
    "nickname": my_nickname,
    "is_rich": is_rich,
    "boob_size": boob_size,
    "bffs": bffs,
}
# you access dict values using its key
name = pawelad["name"]  # -> "Paweł"

# lastly, you can cast variables to other types
a = 22
b = "10"

# c = a + b   <- doesn't work (raises a TypeError exception)
c = a + int(b)  # -> 32

msg = "lat: "
age = 27
# age_msg = msg + age  <- doesn't work (raises a TypeError exception)
age_msg = msg + str(age)  # -> "lat: 27"

# trying to cast values that Python doesn't know how to cast will raise a ValueError exception
# foobar = int(msg)  <- doesn't work (raises a ValueError exception)
