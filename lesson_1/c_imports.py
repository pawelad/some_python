# this imports the 'b_variables' module and allows using all its objects with 'b_variables' prefix
import b_variables

print(b_variables.my_age)
print(b_variables.pawelad)

# this importss 'my_age' and 'pawelad' objects (and only those two) from 'b_variables' module
from b_variables import my_age, pawelad

print(my_age)
print(pawelad)

# this implicitly imports all objects from 'b_variables' module
# it is not recommended, as there's no easy way of telling where given object came from
from b_variables import *  # NOT RECOMMENDED

print(bffs)

# 'random' is a builtin Python package that you can import from
# this imports 'randint' function that returns a random integer from given range
from random import randint

foobar1 = randint(0, 10)  # -> 10
foobar2 = randint(0, 10)  # -> 8
foobar3 = randint(0, 10)  # -> 0
