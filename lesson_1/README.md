# Lesson 1 - Basics

## Hello World
| Code  | [a_hello_world.py](a_hello_world.py)                                                      |
|-------|-------------------------------------------------------------------------------------------|
| Other | https://www.digitalocean.com/community/tutorials/how-to-write-your-first-python-3-program |

Simply put, "Hello World" is the first program every programmer writes <sup>[Citation needed]</sup>.

```console
$ python a_hello_world.py
Hello World
```

It outputs `Hello World`. Nice.

## Variables
| Code  | [b_variables.py](b_variables.py)                   |
|-------|----------------------------------------------------|
| Other | https://www.learnpython.org/en/Variables_and_Types |

The most basic element of (almost?) every computer program are **variables**. We assign _values_ to
_variables_ and then, you know, use them. Variables can be passed to _functions_ to generalize a
given set of operations.

You assign a variable with `=` operator: i.e. `year = 2022`.

Each _variable_ (and thus, each _value_) has a _type_. For example, `"hi there"` is a string
(`str`) and `1` is an integer (`int`).

Certain operations can be performed only on certain types - for example you can do `1 + 1`  but you
cannot do `1 + 'foobar'` (that _raises_ an _exception_, but more on that later).

You can find out variable type by builtin functions `type()` and `isinstance()`.

Most important builtin types:
- integer (i.e. `72`)
- float (i.e. `22.5`)
- string (i.e. `'hi there'`)
- boolean (i.e. `True` or `False`)
- list (i.e. `[1, 2, 3, True, "foobar"]`)
- dictionary (i.e. `{1: "foobar", 2: True}`)
- set (i.e. `{1, 5, 7, 42, 69}`)
- `None` (which is _technically_ a value, with the type being `NoneType`, but just focus on the
  `None` part)

You can think about `None` as "0, but for programming values". Kinda.

You can also create your own types (via custom _classes_), but more about that later.

There's a difference between an integer `1` and a string `'1'` and they are **not** interchangeable.
Changing type of variable (or rather, the variable's _value_) is called _casting_. For example, you
can cast string `'1'` to integer `1` by calling the builtin `int` function: `int('1')`

Casting some values (i.e. `int("foobar")`) will also error out and _raise_ an _exception_.

## Imports
| Code  | [c_imports.py](c_imports.py) |
|-------|------------------------------|
| Other |                              |

while Python itself doesn't care if your program consists of 1 file with 1000 lines of code, or 10
files with 100 lines of code in each, developers certainly do. It's much easier to navigate a
codebase if it's well organized into separate _modules_ (each file ending in `.py` extension is a
_module_), which can then be grouped into _packages_ (a directory that contains _modules_ is
considered a _package_).

You can import (exported) objects between modules using the `import` statement, i.e.
`from b_variables import my_age`. Variable (or rather an _object_, as it could also be a function
or a class) called `my_age` is now available to you.

You can also import things from Python's standard library (i.e. `from random import randint`) or
third party packages that you installed on your computer (more on that later).

## Boolean logic
| Code  | [d_boolean_logic.py](d_boolean_logic.py)                                                 |
|-------|------------------------------------------------------------------------------------------|
| Other | https://www.digitalocean.com/community/tutorials/understanding-boolean-logic-in-python-3 |

Every value and _statement_ can be evaluated (cast to) a `boolean` value (either _True_ or _False_).
Operator `==` is used to compare _values_ (`is` is used for comparing _object identity_ but more
about it later). For example, `1 == 1` and `5 > 1` both evaluate to `True` and `5 == 1` evaluates
to `False`.

You can evaluate (cast) _values_ to a boolean by using `bool()`. Some values are considered _truthy_
or _falsy_. For example, an empty string (`""`) is considered _falsy_ (so `bool("")` evaluates to
`False`), and a non-empty (i.e. `"foobar"`) string is considered _truthy_ (so `bool("foobar")`
evaluates to `True`).

To check if a _value_ is present in a list (container), use the `in` keyword, i.e.
`5 in [1, 2, 3, 4]`.

Two other keywords used for logical operations are `or` and, well, `and`. Given variables `x` and
`y`, `x and y` is _only_ `True` when _both_ `x` and `y` are `True`; `x or y` is _only_ `False` when
_both_ `x` and `y` are `False`. But, like they say, a ~picture~ table is worth a thousand words:

| x     | y     | x and y | x or y |
|-------|-------|---------|--------|
| False | False | False   | False  |
| False | True  | False   | True   |
| True  | False | False   | True   |
| True  | True  | True    | True   |

Conditional statements allow us to specify blocks of code that only happen if a condition is met
(or rather, if it evaluates to `True`). Every if statement _must_ have and `if` block with a
_condition_, _can_ have multiple `elif` blocks with other _conditions_, and _can_  have an `else`
block that will be entered if none of conditions were met.

For example:

```python
happiness = 7.5
if happiness <= 3:
    print("😭")
elif 3 < happiness <= 6:
    print("😶")
elif 7 < happiness <= 9:
    print("😁")
else:
    print("🔥")
```

## Homework
Since we just started, no homework for you! Yay!
