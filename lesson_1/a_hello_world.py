# This is a comment
# You can write anything here and Python just skips over it when running the program

# And this is your first program! It passes "Hello World" string to 'print' function.
# Congrats! ✨
print("Hello World")
