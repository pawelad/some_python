"""
Simple game that first picks a random number and then asks the user to guess it.
"""
from random import randint

RANGE_START = 1
RANGE_STOP = 100

n = randint(RANGE_START, RANGE_STOP)

while True:
    print(f"Enter number between {RANGE_START} and {RANGE_STOP}:")
    user_guess = input()

    try:
        user_guess = int(user_guess)
    except ValueError:
        print(f"'{user_guess}' is not a number")
        continue

    print(f"Your guess: {user_guess}")

    if user_guess == n:
        print("Congrats! ✨")
        break
    elif RANGE_START > user_guess > RANGE_STOP:
        print(f"Number should be between {RANGE_START} and {RANGE_STOP}")
    elif user_guess < n:
        print("Too small")
    elif user_guess > n:
        print("Too big")
    else:
        print("Try again...")
