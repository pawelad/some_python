# Lesson 3 - Mini project

## Guess! The! Number!
OK, now you know the building blocks used in all Python programs, but they may seem very abstract
right now. Like, you now know what a loop is, but _when_ should you use it?

So, let's try to make a program that actually _does_ something.

Let's make a simple game:
- generate a random number between 1 and 10
- let the user guess what number was generated (note: remember that `input` always returns a string)
- if they're right, congratulate them and exit
- if they're not right, ask again until they're right

If all that went great, you can also:
- let the user know if they entered a number outside the generation range
- increase the range of random numer generation to 100
- let the user know if the number they guessed is higher or lower than the generated number

If you still want to do more, you can also limit the number of guesses the user has.

If you're having problems, you can take a look at [a_guess_number.py](a_guess_number.py) file.
